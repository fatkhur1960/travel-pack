<section style="background-color: #f6f6f6;" class="fasilitas">
        <div class="container">
            <div class="section-wrapper">
                <div class="section-title">
                    <h2>Penawaran Terbaik Kami</h2>
                    <p>Tentunya kami akan membuat perjalanan Anda menjadi lebih mudah</p>
                </div>
                <div class="section-main">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="card-icon">
                                                <i class="fa fa-hand-holding-usd"></i>
                                            </div>
                                            <h3 class="card-title">Harga Termurah</h3>
                                            <p class="card-detail">Class aptent taciti sociosutn tora torquent conub nost reptos himenaeos. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="card-icon">
                                                <i class="fa fa-building"></i>
                                            </div>
                                            <h3 class="card-title">Hotel Kelas Atas</h3>
                                            <p class="card-detail">Class aptent taciti sociosutn tora torquent conub nost reptos himenaeos. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="card-icon">
                                                <i class="fa fa-car"></i>
                                            </div>
                                            <h3 class="card-title">Transportasi Nyaman</h3>
                                            <p class="card-detail">Class aptent taciti sociosutn tora torquent conub nost reptos himenaeos. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="card-icon">
                                                <i class="fa fa-hands"></i>
                                            </div>
                                            <h3 class="card-title">Keamanan Terjamin</h3>
                                            <p class="card-detail">Class aptent taciti sociosutn tora torquent conub nost reptos himenaeos. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>