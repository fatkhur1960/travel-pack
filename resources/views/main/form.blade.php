<section class="main" id="main">
        <div class="container">
            <div class="section-wrapper">
                <div class="section-title">
                    <h2>Temukan Perjalanan Anda</h2>
                    <p>Ke mana Anda akan pergi?</p>
                </div>
                <div class="section-main">
                    <form class="form-inline" method="post" action="{{ route('send.request') }}">
                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="col">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-archive"></i></div>
                                    </div>
                                    <select class="custom-select{{ $errors->has('paket') ? ' is-invalid' : '' }}" name="paket">
                                        <option>-- Pilih Paket --</option>
                                        @foreach($pkg as $p)
                                        <option value="{{ $p->package_id }}">{{ $p->package_name }} - {{ $p->package_price }}/orang</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('paket'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('paket') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-users"></i></div>
                                    </div>
                                    <input type="number" name="peserta" class="form-control{{ $errors->has('peserta') ? ' is-invalid' : '' }}" placeholder="Jumlah Peserta">
                                    @if ($errors->has('peserta'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('peserta') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-calendar-alt"></i></div>
                                    </div>
                                    <input id="datepicker" type="text" name="tanggal" class="form-control{{ $errors->has('tanggal') ? ' is-invalid' : '' }}" placeholder="Tanggal">
                                    @if ($errors->has('tanggal'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('tanggal') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-primary">Kirim</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>