<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <span class="colored"><i class="fa fa-bullhorn"></i> Guide</span>Indo
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="{{ url('/') }}">
                        {{ __('Beranda') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="{{ url('destinasi-wisata') }}">
                        {{ __('Destinasi Wisata') }}
                    </a>
                </li>
                <li class="nav-item">
                        <a class="nav-link waves-effect waves-light" href="{{ url('about-us') }}">
                            {{ __('Tentang Kami') }}
                        </a>
                    </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="{{ url('contact-us') }}">
                        {{ __('Hubungi Kami') }}
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <i class="fa fa-user"></i>
                        @if(auth()->guard('member')->check())
                        {{ __('Hai, ')}} {{ auth()->guard('member')->user()->firstname }}
                        @else
                        {{ __('Akun') }}
                        @endif
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                        @if(!auth()->guard('member')->check())
                        <a class="dropdown-item waves-effect waves-light" href="{{ url('client/login') }}">{{ __('Masuk') }}</a>
                        <a class="dropdown-item waves-effect waves-light" href="{{ url('client/register') }}">{{ __('Daftar') }}</a>
                        @else
                        <a class="dropdown-item waves-effect waves-light" href="{{ url('client') }}">{{ __('Dashboard') }}</a>
                        <a class="dropdown-item waves-effect waves-light" href="{{ url('client/setting') }}">{{ __('Pengaturan Akun') }}</a>
                        <a class="dropdown-item waves-effect waves-light" href="{{ url('client/logout') }}">{{ __('Keluar') }}</a>
                        @endif
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>