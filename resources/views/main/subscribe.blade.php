<section class="main" style="background-color: #f5f5f5;">
    <div class="container">
        <div class="section-wrapper">
            <div class="section-title">
                <h2>Dapatkan Kabar Terbaru</h2>
                <p>Daftarkan email Anda untuk mendapatkan informasi tempat wisata terbaru</p>
            </div>
            <div class="section-main">
                <form class="form-inline" method="post" action="{{ route('subscribe') }}">
                    {{ csrf_field() }}
                    <div class="form-row">
                        <div class="col">
                            <div class="input-group">
                                <input type="text" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <button type="submit" class="btn btn-info">Daftar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>