<footer>
    <div class="footer-content">
        <div class="container">
            <div class="row">
                <div class="footer-col col-lg-5 col-md-7 col-12">
                    <div class="footer-col-inner">
                        <h3 class="title">Tentang Kami</h3>
                        <p>Maecenas ac vehicula velit, nec facilisis elit. Phasellus non porttitor justo, eu bibendum elit. Maecenas pharetra non ligula quis ultricies. Nulla varius vestibulum ligula quis hendrerit. Maecenas et fermentum massa. Ut hendrerit, nulla fringilla venenatis pulvinar, nisl est adipiscing nunc, quis consequat mi mauris vel felis.</p>
                        <p><a class="more" href="#">Learn more <i class="fa fa-arrow-right"></i></a></p>
                    </div>
                </div>
                <div class="footer-col col-lg-3 col-md-4 col-12 mr-lg-auto">
                    <h3 class="title">Halaman</h3>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-caret-right"></i>Knowledge Base</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i>Jobs</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i>Press</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i>Terms of services</a></li>
                        <li><a href="#"><i class="fa fa-caret-right"></i>Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="footer-col col-lg-3 col-12 contact">
                    <h3 class="title">Hubungi Kami</h3>
                    <div class="row">
                        <p class="tel col-lg-12 col-md-4 col-12"><i class="fa fa-phone"></i>{{ get_option('phone') }}</p>
                        <p class="email col-lg-12 col-md-4 col-12"><i class="fa fa-envelope"></i><a href="#">{{ get_option('email') }}</a></p>
                        <p class="email col-lg-12 col-md-4 col-12"><i class="fa fa-comment"></i><a href="#">Live Chat</a></p>    
                    </div>    
                </div>                
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <p>&copy; Copyright {{ date('Y') }} {{ config('app.name') }}. Allrights Reserved.</p>
        </div>
    </div>
</footer>