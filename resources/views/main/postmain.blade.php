<section>
    <div class="container">
        <div class="section-wrapper">
            <div class="section-title">
                <h2>Tempat Wisata Pilihan</h2>
                <p>Kami akan membuat wisata Anda menjadi lebih menyenangkan</p>
            </div>
            <div class="section-main posts post-main">
                <div class="row">
                    @foreach($posts as $post)
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow">
                            <img class="card-img-top" src="{{ url( Image::url($post->post_thumbnail, 300, 200, array('crop')) ) }}">
                            <div class="card-body">
                                <a class="card-title" href="{{ url('destinasi-wisata/' . $post->post_slug) }}">{{ $post->post_title }}</a>
                                <p class="card-text">
                                    {!! excerpt($post->post_body, 100) !!}
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-md-12 d-flex justify-content-center">
                        <a class="btn btn-info" href="{{ route('view.more') }}">Lihat lebih banyak</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>