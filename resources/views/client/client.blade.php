@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
<section class="section-wrapper">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard - Histori Pemesanan Paket</div>
                <div class="card-body">
                    @if (\Session::has('warning'))
                        <div class="alert alert-warning">
                            {{ \Session::get('warning') }}
                        </div>
                    @endif

                    @if (\Session::has('message'))
                        <div class="alert alert-success">
                            {{ \Session::get('message') }}
                        </div>
                    @endif

                    <table class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th width="200">Paket</th>
                                <th>Tanggal</th>
                                <th>Peserta</th>
                                <th>Harga</th>
                                <th>Total</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($inv as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->package_name }}</td>
                                <td>{{ $item->date }}</td>
                                <td>{{ $item->volume }}</td>
                                <td>{{ $item->package_price }}/orang</td>
                                <td>{{ $item->total_price }}</td>
                                <td width="120">
                                    @if($item->status == 'pending')
                                        <a data-id="{{ $item->invoice_id }}" data-toggle="modal" data-target="#exampleModal" title="Klik untuk Konfirmasi Pembayaran" href="#" class="badge badge-warning">{{ __('Pending') }}</a>
                                    @else
                                        <span class="badge badge-success">{{ __('Terbayar') }}</span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('Konfirmasi Pembayaran') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="upload" action="{{ route('confirm.order') }}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="invoice_id">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Upload Bukti Pembayaran:</label>
                        <input type="file" name="confirm_file" class="form-control" accept="image/*">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button id="submit" type="button" class="btn btn-primary">Kirim</button>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
