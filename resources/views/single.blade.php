@extends('layouts.app')
@section('title', $post->post_title)
@section('content')
<section class="post-thumbnail" style="background-image: url('{{ url($post->post_thumbnail) }}')">
    <div class="post-title animated fadeInDown">
        <h1>{{ $post->post_title }}</h1>
    </div>
</section>
<section>
    <div class="container">
        <div class="section-wrapper">
            <div class="d-flex justify-content-center">
                <div class="col-md-9">
                    {!! $post->post_body !!}
                </div>
            </div>
        </div>
    </div>
</section>
@include('main.footer')
@endsection