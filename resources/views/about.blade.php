@extends('layouts.app')
@section('title', 'Tentang Kami')
@section('content')
<section>
    <div class="container">
        <div class="section-wrapper">
            <div class="section-title">
                <h2>Tentang Kami</h2>
            </div>
            <div class="section-main d-flex justify-content-center">
                <div class="col-md-9">
                    {{ get_option('tentang') }}
                </div>
            </div>
        </div>
    </div>
</section>
@include('main.footer')
@endsection