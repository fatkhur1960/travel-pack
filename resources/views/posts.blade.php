@extends('layouts.app')
@section('title', 'Destinasi Wisata')
@section('content')
<section>
    <div class="container">
        <div class="section-wrapper">
            <div class="section-title">
                <h2>Destinasi Wisata</h2>
            </div>
            <div class="section-main posts">
                <div class="row d-flex justify-content-center">
                    @foreach($posts as $post)
                    <div class="card col-md-10 box-shadow animated fadeInLeft delayp1" style="padding: 0px;">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{ url( Image::url($post->post_thumbnail, 300, 250, array('crop')) ) }}">
                            </div>
                            <div class="card-body col-md-8">
                                <h3><a href="{{ url('destinasi-wisata/' . $post->post_slug) }}">{{ $post->post_title }}</a></h3>
                                <p class="card-text">
                                    {!! excerpt($post->post_body, 230) !!}
                                </p>
                                <p>
                                    <a href="{{ url('destinasi-wisata/' . $post->post_slug) }}">{{ __('Read more...') }}</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-md-12 d-flex justify-content-center">
                        {{ $posts->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('main.footer')
@endsection