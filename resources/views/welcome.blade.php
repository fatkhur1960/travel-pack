@extends('layouts.app')
@section('title', 'Tour and Travel Agency')
@section('content')

<div class="header blur">
    @if (Route::getCurrentRoute()->uri() == '/')
    @include('main.navbar')
    @endif
    <div class="header-main animated fadeInDown">
        <h1><span class="colored">Guide</span>Indo<br/>{{ __('Tour and Travel Agency') }}</h1>
        <p>
            Agen tour and travel terbaik dan terpercaya
            <a id="start" href="#main" class="animated bounce infinite">
                <i class="fa fa-angle-down"></i>
            </a>
        </p>
    </div>
</div>

@include('main.form')

@include('main.fasilitas')

@include('main.postmain')

@include('main.subscribe')

@include('main.footer')
@endsection
