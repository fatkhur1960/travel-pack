<!Doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('app.name') }} - @yield('title')</title>
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/fontawesome-all.css')}}">
        <link data-prerender="keep" rel="icon" type="image/png" sizes="16x16" href="/img/favicon.png">
        <style type="text/css">
        a:click, a:focus, a:after {
            border: none;
            outline: none !important;
        }
        .list-group .list-group .list-group-item {
            background-color: #efefef;
        }
        .control-label {
            padding-top: 7px;
            margin-bottom: 0;
            text-align: right;
        }
        .list-group .list-group .list-group-item:first-child, .list-group .list-group .list-group-item:last-child {
            border-radius: 0px;
        }
        ul.navbar-nav li {
            display: inline-table;
        }
        #x {
            display: none;
            width: 25px;
            height: 25px;
            border: none;
            padding: 0px 8px;
            border-radius: 100%;
            position: absolute;
            color: white;
            top: 5px;
            right: 20px;
            cursor: pointer;
            background: url('/img/close.png');
            background-size: 100%;
        }
        #x:hover {
            opacity: .8;
        }
        .footer {
            bottom: 0;
            width: 100%;
            /* Set the fixed height of the footer here */
            height: 60px;
            line-height: 60px; /* Vertically center the text there */
            background-color: #f5f5f5;
        }
        .footer > .container {
            padding-right: 15px;
            padding-left: 15px;
        }
        .colored {
            color: #60c9eb;
        }
        .navbar {
            padding: .5rem 1rem;
            box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.5);
        }
        </style>
        <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/tinymce/tinymce.min.js')}}"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <a class="navbar-brand" href="{{ url('dashboard') }}">
                    <span class="colored"><i class="fa fa-bullhorn"></i> Guide</span>Indo
                </a>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user"></i> Hai, {{ Auth::guard('admin')->user()->firstname }} {{ Auth::guard('admin')->user()->lastname }}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" target="_blank" href="{{ url('/') }}">Lihat Situs</a>
                            <a class="dropdown-item" href="{{ url('dashboard/setting') }}">Pengaturan Akun</a>
                            <a class="dropdown-item" href="{{ url('dashboard/logout') }}">Keluar</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-12"><br/></div>
                <div class="col-6 col-md-3">
                    <div class="accordion" id="accordionExample">
                        <div class="list-group">
                            <a href="{{ url('dashboard') }}" class="list-group-item list-group-item-action">
                                <i class="fa fa-home"></i>&nbsp;&nbsp;Dashboard
                            </a>
                            <a href="#" class="list-group-item list-group-item-action" data-toggle="collapse" data-target="#mhs" aria-expanded="true" aria-controls="mhs">
                            <i class="fa fa-box-open"></i>&nbsp;&nbsp;Paket Wisata
                            </a>
                            <div id="mhs" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="list-group">
                                    <a href="{{ url('packages') }}" class="list-group-item list-group-item-action">Lihat Paket</a>
                                    <a href="{{ url('packages/create') }}" class="list-group-item list-group-item-action">Tambah Paket</a>
                                </div>
                            </div>

                            <a href="#" class="list-group-item list-group-item-action" data-toggle="collapse" data-target="#makul" aria-expanded="true" aria-controls="makul">
                            <i class="fa fa-map"></i>&nbsp;&nbsp;Destinasi Wisata
                            </a>
                            <div id="makul" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="list-group">
                                    <a href="{{ url('posts') }}" class="list-group-item list-group-item-action">Lihat Destinasi</a>
                                    <a href="{{ url('posts/create') }}" class="list-group-item list-group-item-action">Tambah Destinasi</a>
                                </div>
                            </div>

                            <a href="#" class="list-group-item list-group-item-action" data-toggle="collapse" data-target="#users" aria-expanded="true" aria-controls="users">
                                <i class="fa fa-users"></i>&nbsp;&nbsp;Admin
                            </a>
                            <div id="users" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="list-group">
                                    <a href="{{ url('users') }}" class="list-group-item list-group-item-action">Lihat Admin</a>
                                    <a href="{{ url('users/create') }}" class="list-group-item list-group-item-action">Tambah Admin</a>
                                </div>
                            </div>

                            <a href="{{ url('member') }}" class="list-group-item list-group-item-action">
                                <i class="fa fa-users"></i>&nbsp;&nbsp;Member
                            </a>

                            <a href="#" class="list-group-item list-group-item-action" data-toggle="collapse" data-target="#settings" aria-expanded="true" aria-controls="users">
                                <i class="fa fa-cogs"></i>&nbsp;&nbsp;Pengaturan Situs
                            </a>
                            <div id="settings" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="list-group">
                                    <a href="{{ url('settings') }}" class="list-group-item list-group-item-action">Lihat Pengaturan</a>
                                    <a href="{{ url('settings/create') }}" class="list-group-item list-group-item-action">Tambah Pengaturan</a>
                                </div>
                            </div>

                            <a href="{{ url('dashboard/logout') }}" class="list-group-item list-group-item-action"><i class="fa fa-sign-out-alt"></i>&nbsp;&nbsp;Keluar</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-9">
                    <div class="card">
                        <div class="card-header">@yield('title')</div>
                        <div class="card-body">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br/><br/><br/><br/>

        <footer class="footer">
            <div class="container">
            <span class="text-muted">&copy; Copyright {{ date('Y') }} {{ config('app.name') }}. All rights reserved.</span>
            </div>
        </footer>

        <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('input#external').click(function() {
                    if($(this).prop('checked')) {
                        $('input[name="img_file"]').hide();
                        $('input[name="thumbnail"]').show();
                    } else {
                        $('input[name="thumbnail"]').hide();
                        $('input[name="img_file"]').show();
                    }
                });

                $('input[name="img_file"]').change(function(){
                    readURL(this);
                });

                $('input[name="thumbnail"]').change(function(){
                    $('#x').show();
                    $('#preview').attr('src', $(this).val());
                });

                $('#x').click(function() {
                    $('input[name="thumbnail"]').val("");
                    $('input[name="img_file"]').val("");
                    $('#preview').attr('src', '');
                    $(this).hide();
                });
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#x').show();
                        $('#preview').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            };

            $('#confirmModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var data = button.data('json');
                
                $.each(data, function(index, item) {
                    $('input[name="invoice_id"]').val(item.id);
                    $('img#img').attr('src',item.image);
                });
                
                $('button#submit').click(function() {
                    $('form#updateStatus').submit();
                });
            });
        </script>
    </body>
</html>