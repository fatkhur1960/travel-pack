<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }} - @yield('title')</title>

        <link data-prerender="keep" rel="icon" type="image/png" sizes="16x16" href="/img/favicon.png">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,500,700" rel="stylesheet" type="text/css">
        
        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/fontawesome-all.css')}}">
        <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/custom.css')}}">
        <link rel="stylesheet" href="{{asset('css/animate.css')}}">
        <style type="text/css">
            body a:focus, a:after {
                border: none;
                outline: none;
                text-decoration: none;
            }
        </style>
    </head>
    <body>
        @if (Route::getCurrentRoute()->uri() != '/')
        @include('main.navbar')
        @endif
        <main>
            @yield('content')
        </main>
        <a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>
        <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
        <script type="text/javascript">
            $('.header').css('min-height',$(window).height());
            if (window.location.pathname == '/') {

                var stickyRibbonTop = $('nav.navbar').offset().top;
                $('.posts .card').hide();
            
                $(window).scroll(function(){
                    if( $(window).scrollTop() > stickyRibbonTop ) {
                        $('nav.navbar').addClass('fixed-top');
                    } else {
                        $('nav.navbar').removeClass('fixed-top');
                    }
                });
            } else {
                $('nav.navbar').addClass('sticky-top');
            }
            
            $(window).scroll(function(){
                var stickyRibbonTop = $('.fasilitas').offset().top-400;
                if( $(window).scrollTop() > stickyRibbonTop ) {
                    $('.fasilitas .card').addClass('animated fadeInUp delayp1');
                }

                if( $(window).scrollTop() > $('.posts').offset().top-550 ) {
                    $('.posts .card').show();
                    $('.posts .card').addClass('animated fadeInUp delayp1');
                }

                if ($(this).scrollTop() >= 50) { 
                    $('#return-to-top').fadeIn(200); 
                } else {
                    $('#return-to-top').fadeOut(200);
                }
            });

            $('#return-to-top').click(function() { 
                $('body,html').animate({
                    scrollTop : 0                  
                }, 1000);
            });

            $('a#start').click(function(event) {
                var target = $(this.hash);
                target = target.length ? target: $('[name=' + this.hash.slice(1) + ']');
                if(target.length) {
                    event.preventDefault();
                    var navHeight = $('.navbar').height();
                    $('html, body').animate({
                        scrollTop: (target.offset().top-navHeight)
                    }, 1000, function() {
                        var $target = $(target);
                        if($target.is(':focus')) {
                            return false;
                        } else {
                            $target.attr('tabindex','-1');
                            $target.focus();
                        }
                    });
                }

                return false;
            });

            $('#datepicker').datepicker({
                format: 'yyyy-mm-dd',
                startDate: '-3d'
            });
            $('#exampleModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var invoice_id = button.data('id');
                $('input[name="invoice_id"]').val(invoice_id);
                
                $('button#submit').click(function() {
                    $('form#upload').submit();
                });
            });
        </script>
    </body>
</html>