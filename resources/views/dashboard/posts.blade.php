@extends('layouts.dashboard')

@section('title', 'Destinasi Wisata')

@section('content')
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {{ \Session::get('success') }}
    </div>
    @endif

    <table class="table table-striped table-sm">
        <thead>
            <tr>
                <th>No</th>
                <th>Destinasi Wisata</th>
                <th>Tanggal Dibuat</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($posts as $m)
            <tr>
                <td>{{$no++}}</td>
                <td>{{$m['post_title']}}</td>
                <td>{{$m['created_at']}}</td>
                <td>
                    <form action="{{action('PostsController@destroy', $m['post_id'])}}" method="post">
                    {{csrf_field()}}
                        <a href="{{action('PostsController@edit', $m['post_id'])}}"
                    class="btn btn-sm btn-warning">Ubah</a>
                        <input name="_method" type="hidden" value="DELETE">
                        <button onclick="if(confirm('Anda yakin ingin menghapus data ini?')) {return true;} else {return false;};" class="btn btn-sm btn-danger" type="submit">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br/>
{{ $posts->links() }}

@endsection