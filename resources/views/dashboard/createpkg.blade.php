@extends('layouts.dashboard')

@section('title', 'Tambah Paket')

@section('content')

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {{ \Session::get('success') }}
    </div>
    @endif

    <form action="{{ url('packages') }}" method="POST" class="form-horizontal">
        {{csrf_field()}}
        <div class="row form-group">
            <label for="package_name" class="col-md-3 control-label">Nama Paket</label>
            <div class="col-md-7">
                <input type="text" class="form-control" name="package_name">
            </div>
        </div>
        <div class="row form-group">
            <label for="package_price" class="col-md-3 control-label">Harga</label>
            <div class="col-md-7">
                <input type="text" class="form-control" name="package_price">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-7">
                <button type="submit" class="btn btn-success">Simpan</button>   
            </div>
        </div>
    </form>

@endsection