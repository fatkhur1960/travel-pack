<!-- index.blade.php -->

@extends('layouts.dashboard')

@section('title', 'Paket Wisata')

@section('content')
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {{ \Session::get('success') }}
    </div>
    @endif

    <table class="table table-striped table-sm">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Paket</th>
                <th>Harga</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($packages as $m)
            <tr>
                <td>{{$no++}}</td>
                <td>{{$m['package_name']}}</td>
                <td>{{$m['package_price']}}</td>
                <td>
                    <form action="{{action('PackagesController@destroy', $m['package_id'])}}" method="post">
                    {{csrf_field()}}
                        <a href="{{action('PackagesController@edit', $m['package_id'])}}"
                    class="btn btn-sm btn-warning">Ubah</a>
                        <input name="_method" type="hidden" value="DELETE">
                        <button onclick="if(confirm('Anda yakin ingin menghapus data ini?')) {return true;} else {return false;};" class="btn btn-sm btn-danger" type="submit">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br/>
{{ $packages->links() }}

@endsection