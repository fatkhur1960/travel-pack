<!-- index.blade.php -->

@extends('layouts.dashboard')

@section('title', 'Paket Wisata')

@section('content')
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {{ \Session::get('success') }}
    </div>
    @endif

    <table class="table table-striped table-sm">
        <thead>
            <tr>
                <th>No</th>
                <th>Username</th>
                <th>Email</th>
                <th>Nama Lengkap</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $m)
            <tr>
                <td>{{$no++}}</td>
                <td>{{$m['username']}}</td>
                <td>{{$m['email']}}</td>
                <td>{{$m['firstname']}} {{$m['lastname']}}</td>
                <td>
                    <form action="{{action('UsersController@destroy', $m['user_id'])}}" method="post">
                    {{csrf_field()}}
                        <a href="{{action('UsersController@edit', $m['user_id'])}}"
                    class="btn btn-sm btn-warning">Ubah</a>
                        <input name="_method" type="hidden" value="DELETE">
                        <button onclick="if(confirm('Anda yakin ingin menghapus user ini?')) {return true;} else {return false;};" class="btn btn-sm btn-danger" type="submit">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br/>
{{ $users->links() }}

@endsection