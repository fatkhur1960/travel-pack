@extends('layouts.dashboard')

@section('title', 'Dashboard')

@section('content')

@if (\Session::has('warning'))
    <div class="alert alert-warning">
        {{ \Session::get('warning') }}
    </div>
@endif

@if (\Session::has('success'))
    <div class="alert alert-success">
        {{ \Session::get('success') }}
    </div>
@endif

<table class="table table-striped table-sm">
    <thead>
        <tr>
            <th>No</th>
            <th>Paket</th>
            <th>Pemesan</th>
            <th>Tanggal</th>
            <th>Peserta</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $item)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $item->package_name }}</td>
            <td>{{ $item->email }}</td>
            <td>{{ $item->date }}</td>
            <td>{{ $item->volume }}</td>
            <td>
                @if($item->status == 'pending')
                    <a data-json="{{ json_encode([['id' => $item->invoice_id, 'image' => $item->file_location]]) }}" data-toggle="modal" data-target="#confirmModal" title="Klik untuk Konfirmasi Pembayaran" href="#" class="badge badge-warning">{{ __('Pending') }} {{ $item->total }}</a>
                @else
                    <span class="badge badge-success">{{ __('Terbayar') }}</span>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('Konfirmasi Pembayaran') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="updateStatus" action="{{ route('confirm.update') }}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="invoice_id">
                    <div class="d-flex justify-content-center">
                    	<img src="" id="img" style="max-height: 350px;" class="img-thumbnail" alt="Responsive image">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button id="submit" type="button" class="btn btn-primary">Konfirmasi</button>
            </div>
        </div>
    </div>
</div>

@endsection