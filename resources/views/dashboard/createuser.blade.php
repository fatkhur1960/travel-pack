@extends('layouts.dashboard')

@section('title', 'Tambah User')

@section('content')

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {{ \Session::get('success') }}
    </div>
    @endif

    <form action="{{ url('users') }}" method="POST" class="form-horizontal">
        {{csrf_field()}}
        <div class="row form-group">
            <label for="username" class="col-md-3 control-label">Username</label>
            <div class="col-md-7">
                <input type="text" class="form-control" name="username">
            </div>
        </div>
        <div class="row form-group">
            <label for="password" class="col-md-3 control-label">Password</label>
            <div class="col-md-7">
                <input type="password" class="form-control" name="password">
            </div>
        </div>
        <div class="row form-group">
            <label for="email" class="col-md-3 control-label">Email</label>
            <div class="col-md-7">
                <input type="text" class="form-control" name="email">
            </div>
        </div>
        <div class="row form-group">
            <label for="firstname" class="col-md-3 control-label">Nama Depan</label>
            <div class="col-md-7">
                <input type="text" class="form-control" name="firstname">
            </div>
        </div>
        <div class="row form-group">
            <label for="lastname" class="col-md-3 control-label">Nama Belakang</label>
            <div class="col-md-7">
                <input type="text" class="form-control" name="lastname">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-7">
                <button type="submit" class="btn btn-success">Simpan</button>   
            </div>
        </div>
    </form>

@endsection