@extends('layouts.dashboard')

@section('title', 'Tambah Pengaturan')

@section('content')

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {{ \Session::get('success') }}
    </div>
    @endif

    <form action="{{ action('OptionsController@update', $item->option_id) }}" method="POST" enctype="multipart/form-data" >
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="option_name">Nama</label>
                            <input type="text" class="form-control" name="option_name" value="{{ $item->option_name }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="option_name">Tipe</label>
                            <select name="option_type" class="custom-select">
                                <option {{ ($item->option_type == 'html') ? 'selected="true"' : "" }} value="html">html</option>
                                <option {{ ($item->option_type == 'text') ? 'selected="true"' : "" }} value="text">text</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-12">
                <label for="option_value">Konten</label>
                <textarea class="form-control" id="rte" name="option_value" rows="10">{{ $item->option_value }}</textarea>
                <script type="text/javascript">
                    tinymce.init({ 
                        selector:'textarea#rte', 
                        width: 780, 
                        height: 320,
                        plugins: ["fullscreen","code"],
                        menubar: false,
                        toolbar: "undo redo | styleselect | bold underline italic | link | alignleft aligncenter alignright | fullscreen | code" 
                    });
                    $('window').load(function() {
                    @if($item->option_type == 'text')
                        tinymce.get('rte').hide();
                    @else
                        tinymce.get('rte').show();
                    @endif
                    });
                </script>
            </div>
            <div class="form-group col-md-12">
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>
        </div>
    </form>

@endsection