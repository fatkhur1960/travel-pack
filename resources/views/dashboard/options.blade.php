<!-- index.blade.php -->

@extends('layouts.dashboard')

@section('title', 'Pengaturan Situs')

@section('content')
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {{ \Session::get('success') }}
    </div>
    @endif

    <table class="table table-striped table-sm">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($options as $item)
            <tr>
                <td>{{$no++}}</td>
                <td>{{$item->option_name}}</td>
                <td>
                    <form action="{{action('OptionsController@destroy', $item->option_id)}}" method="post">
                    {{csrf_field()}}
                        <a href="{{action('OptionsController@edit', $item->option_id)}}"
                    class="btn btn-sm btn-warning">Ubah</a>
                        <input name="_method" type="hidden" value="DELETE">
                        <button onclick="if(confirm('Anda yakin ingin menghapus data ini?')) {return true;} else {return false;};" class="btn btn-sm btn-danger" type="submit">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br/>
    {{ $options->links() }}
@endsection