<!-- index.blade.php -->

@extends('layouts.dashboard')

@section('title', 'Member')

@section('content')
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {{ \Session::get('success') }}
    </div>
    @endif

<table class="table table-striped table-sm">
    <thead>
        <tr>
            <th>No</th>
            <th>Username</th>
            <th>Email</th>
            <th>Nama Lengkap</th>
            <th>Status</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach($members as $m)
        <tr>
            <td>{{$no++}}</td>
            <td>{{$m->username}}</td>
            <td>{{$m->email}}</td>
            <td>{{$m->firstname}} {{$m->lastname}}</td>
            <td>{{$m->status}}</td>
            <td>
                <form action="{{action('MemberController@destroy', $m->member_id)}}" method="post">
                {{csrf_field()}}
                    <input name="_method" type="hidden" value="DELETE">
                    <button onclick="if(confirm('Anda yakin ingin menghapus member ini?')) {return true;} else {return false;};" class="btn btn-sm btn-danger" type="submit">Hapus</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<br/>
{{ $members->links() }}

@endsection