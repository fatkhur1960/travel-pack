@extends('layouts.dashboard')

@section('title', 'Pengaturan Akun')

@section('content')

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {{ \Session::get('success') }}
    </div>
    @endif

    <form action="{{ action('UsersController@update', Auth::guard('admin')->user()->user_id) }}" method="POST" class="form-horizontal">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PATCH">
        <div class="row form-group">
            <label for="username" class="col-md-3 control-label">Username</label>
            <div class="col-md-7">
                <input type="text" class="form-control" name="username" value="{{ Auth::guard('admin')->user()->username }}">
            </div>
        </div>
        <div class="row form-group">
            <label for="password" class="col-md-3 control-label">Password</label>
            <div class="col-md-7">
                <input type="password" class="form-control" name="password">
                <span>*) Biarkan kosong jika tidak ingin diubah</span>
            </div>
        </div>
        <div class="row form-group">
            <label for="email" class="col-md-3 control-label">Email</label>
            <div class="col-md-7">
                <input type="text" class="form-control" name="email" value="{{ Auth::guard('admin')->user()->email }}">
            </div>
        </div>
        <div class="row form-group">
            <label for="firstname" class="col-md-3 control-label">Nama Depan</label>
            <div class="col-md-7">
                <input type="text" class="form-control" name="firstname" value="{{ Auth::guard('admin')->user()->firstname }}">
            </div>
        </div>
        <div class="row form-group">
            <label for="lastname" class="col-md-3 control-label">Nama Belakang</label>
            <div class="col-md-7">
                <input type="text" class="form-control" name="lastname" value="{{ Auth::guard('admin')->user()->lastname }}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-7">
                <button type="submit" class="btn btn-success">Simpan</button>   
            </div>
        </div>
    </form>

@endsection