@extends('layouts.dashboard')

@section('title', 'Tambah Destinasi')

@section('content')

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {{ \Session::get('success') }}
    </div>
    @endif

    <form action="{{ url('posts') }}" method="POST" enctype="multipart/form-data" >
        {{csrf_field()}}
        <div class="row">
            <div class="form-group col-md-12">
                <label for="post_title">Nama Destinasi</label>
                <input type="text" class="form-control" name="post_title">
            </div>
            <div class="form-group col-md-12">
                <label for="post_body">
                    Deskripsi
                    <textarea name="post_body" rows="10"></textarea>
                    <script>
                        tinymce.init({ 
                            selector:'textarea', 
                            width: 780, 
                            height: 320,
                            plugins: "fullscreen",
                            menubar: false,
                            toolbar: "undo redo | styleselect | bold underline italic | link | alignleft aligncenter alignright | fullscreen" 
                        });
                    </script>
                </label>
            </div>
            <div class="form-group col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <label for="thumbnail">Thumbnail</label>
                        <input type="checkbox" id="external"> Eksternal
                        <input type="file" class="form-control" name="img_file" accept="image/*" style="overflow: hidden;"/>
                        <input type="text" style="display: none;" name="thumbnail" class="form-control" placeholder="Masukkan link thumbnail"/>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="row">
                    <div class="col-md-6" style="position: relative;">
                        <a id="x">&nbsp;</a>
                        <img id="preview" class="rounded img-fluid"/>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <button type="submit" class="btn btn-success">Simpan</button>
            </div>
        </div>
    </form>

@endsection