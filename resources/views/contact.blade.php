@extends('layouts.app')
@section('title', 'Hubungi Kami')
@section('content')
<section>
    <div class="container">
        <div class="section-wrapper">
            <div class="section-title">
                <h2>Hubungi Kami</h2>
            </div>
            <div class="contact">
                @if(session()->has('message'))
                    <div class="alert alert-info">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="row d-flex justify-content-center">
                    <div class="card col-md-3 col-sm-12 col-xs-12">
                        <div class="card-body">
                            <div class="icon animated fadeInUp delayp1">
                                <i class="fa fa-envelope-open"></i>
                            </div>
                            <div class="details">
                                <h4 class="title">Email</h4>
                                <p><a href="#">{{ get_option('email') }}</a></p>
                            </div><!--details-->
                        </div><!--//card-body-->
                    </div><!--//card-->
                    <div class="card col-md-3 col-sm-12 col-xs-12">
                        <div class="card-body">
                            <div class="icon animated fadeInUp delayp1">
                                <i class="fa fa-mobile-alt"></i>
                            </div>
                            <div class="details">
                                <h4 class="title">Call us on:</h4>
                                <p class="phone">{{ get_option('phone') }}</p>
                                <p class="day">Mon-Sat 9am-6pm (GMT+7)</p>
                            </div><!--details-->
                        </div><!--//card-body-->
                    </div><!--//card-->
                    <div class="card col-md-3 col-sm-12 col-xs-12 last">
                            <div class="card-body">
                            <div class="icon animated fadeInUp delayp1">
                                <i class="fa fa-map-marker-alt"></i>
                            </div>
                            <div class="details">
                                <h4 class="title">Find us at:</h4>
                                <p class="address">123 Queen Square<br>Bristol, UK</p>
                            </div><!--details-->
                        </div><!--//card-body-->
                    </div><!--//card-->
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="section-title">
            <h2>Kirim Pesan Anda</h2>
        </div>
        <div class="section-main d-flex justify-content-center">
            <div class="col-md-6">
                <form method="post" action="{{ route('send.message') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" required placeholder="Nama" name="name" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}">
                        @if ($errors->has('name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="text" required placeholder="Email" name="email" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <textarea placeholder="Pesan" name="message" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" rows="7"></textarea>
                        @if ($errors->has('message'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('message') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="submit" class="col-md-12 btn btn-info" value="Kirim Pesan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="map-section">
    <div class="gmap-wrapper" id="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d79444.64391671501!2d-0.21428374128957384!3d51.51972634746694!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876045108e9cad3%3A0x6514817fa6d57c9!2sThe+Web+Kitchen!5e0!3m2!1sen!2suk!4v1469624353093" style="border:0;margin: 0px; padding: 0px;" allowfullscreen="" width="100%" height="450" frameborder="0"></iframe>
    </div>
</section>
@include('main.footer')
@endsection