<?php

namespace App\Mail;

use App\Models\Member;
use Crypt;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Member $member)
    {
        $this->member = $member;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $enc = Crypt::encrypt($this->member->email);
        $link = route('clientregister.verify', ['token' => $enc]);

        return $this->subject( config('app.name') . ' - Your Verification Email Address')
            ->with('link', $link)
            ->from(config('mail.username'),'Admin ' . config('app.name'))
            ->view('mail.signup');
    }
}
