<?php

namespace App\Mail;

use App\Models\Member;
use Crypt;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->member = Member::whereRaw("email='$email'")->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->member->reset_token = Crypt::encrypt($this->getToken(5));
        $this->member->save();
        $link = route('client.reset', [
            'token' => $this->member->reset_token,
            'identifier' => Crypt::encrypt($this->member->email)
        ]);

        return $this->subject( config('app.name') . ' - Your Reset Password Link')
            ->with('link', $link)
            ->from(config('mail.username'),'Admin ' . config('app.name'))
            ->view('mail.reset');
    }

    private function getToken($length){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited
   
       for ($i=0; $i < $length; $i++) {
           $token .= $codeAlphabet[random_int(0, $max-1)];
       }
   
       return $token;
   }
}
