<?php

function get_option($name) 
{
	$options = App\Models\Options::where('option_name', '=', $name)->first();
	if($options->option_value)
	{
		echo $options->option_value;
	}
	else
	{
		echo "Tidak ada opsi $name";
	}
}

function excerpt($str, $cutOffLength) 
{

    $charAtPosition = "";
    $strLength = strlen($str);

    do {
        $cutOffLength++;
        $charAtPosition = substr($str, $cutOffLength, 1);
    } while ($cutOffLength < $strLength && $charAtPosition != " ");

    return strip_tags(substr($str, 0, $cutOffLength) . '...');

}