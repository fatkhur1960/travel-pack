<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Options;

class OptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no = 1;
        $options = Options::orderBy('created_at','desc')->paginate('20');
        return view('dashboard.options', compact('options','no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.createoptions');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validate(request(), [
            'option_name' => 'required|unique:options,option_name',
            'option_value' => 'required',
            'option_type' => 'required'
        ]);

        if($request->option_type == 'text') 
        {
            $data['option_value'] = strip_tags($request->option_value);
        }

        Options::create($data);

        return redirect('settings')->with('success','Pengaturan baru telah disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Options::find($id);

        return view('dashboard.editoptions')->with('item', $item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Options::find($id);
        $option = $this->validate(request(), [
            'option_name' => 'required',
            'option_value' => 'required',
            'option_type' => 'required'
        ]);


        if($request->option_type == 'text') 
        {
            $option['option_value'] = strip_tags($request->option_value);
        } 

        $item->option_name = $option['option_name'];
        $item->option_value = $option['option_value'];
        $item->option_type = $option['option_type'];
        $item->save();

        return redirect('settings')->with('success','Pengaturan berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pkg = Options::find($id);
        $pkg->delete();
        return redirect('settings')->with('success','Opsi berhasil dihapus');
    }
}
