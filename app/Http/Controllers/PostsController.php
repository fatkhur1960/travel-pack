<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Posts;
use Session;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no = 1;
        $posts = Posts::orderBy('created_at','desc')->paginate('20');
        return view('dashboard.posts', compact('posts','no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.createpost');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = 'images/';
        $data = $this->validate(request(), [
            'post_title' => 'required',
            'post_body' => 'required'
        ]);
        $data['post_slug'] = str_slug($request->post_title, '-');

        $file = $request->file('img_file');
        if($file) 
        {
            $data['post_thumbnail'] = $path . $file->getClientOriginalName();
            $file->move($path, $file->getClientOriginalName());
        } 
        else
        {
            $data['post_thumbnail'] = $request->get('thumbnail');
        }

        Posts::create($data);

        return redirect('posts')->with('success','Destinasi baru telah disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Posts::find($id);
        return view('dashboard.editpost', compact('post','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Posts::find($id);
        $path = 'images/';
        $this->validate(request(), [
            'post_title' => 'required',
            'post_body' => 'required'
        ]);

        $post->post_title = $request->get('post_title');
        $post->post_body = $request->get('post_body');
        $post->post_slug = str_slug($request->post_title, '-');

        $file = $request->file('img_file');
        if($file || $request->get('thumbnail')) 
        {
            unlink(public_path() . '/' . $post->post_thumbnail);
            if($file)
            {
                $post->post_thumbnail = $path . $file->getClientOriginalName();
                $file->move($path, $file->getClientOriginalName());
            }
            else
            {
                $post->post_thumbnail = $request->get('thumbnail');
            }
        } 
        else
        {
            $post->post_thumbnail = $post->post_thumbnail;
        }

        $post->save();

        return redirect('posts')->with('success','Destinasi telah diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Posts::find($id);
        $post->delete();
        return redirect('posts')->with('success','Destinasi berhasil dihapus');
    }
}
