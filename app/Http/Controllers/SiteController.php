<?php

namespace App\Http\Controllers;

use Session;
use Auth;
use Cart;
use Mail;
use App\Mail\SendMessage;
use Illuminate\Http\Request;
use App\Models\Packages;
use App\Models\Posts;

class SiteController extends Controller
{
    public function index()
    {
        $pkg = Packages::orderBy('package_name','asc')->get();
        $posts = Posts::orderBy('post_id','desc')->paginate(3);
        return view('welcome', compact('pkg','posts'));
    }

    public function posts()
    {
        $posts = Posts::orderBy('post_id','desc')->paginate(10);
        if($posts) 
        {
            return view('posts', compact('posts'));
        }
        else
        {
            return abort(404);
        }
    }

    public function viewPost($slug)
    {
        $post = Posts::where('post_slug','=',$slug)->first();
        if($post)
        {
            return view('single')->with('post', $post);
        }
        else
        {
            return abort(404);
        }
    }

    public function send_request(Request $request)
    {
        $data = $this->validate(request(), [
            'paket' => 'required',
            'tanggal' => 'required',
            'peserta' => 'required'
        ]);

        $pkg = Packages::find($data['paket']);
        Session::put('redirect', 'client/checkout');

        Cart::add([
            'id' => $pkg->package_id,
            'name' => $pkg->package_name,
            'qty' => $data['peserta'],
            'price' => $pkg->package_price,
            'options' => ['date' => $data['tanggal']]
        ]);

        if(Auth::guard('member')->check())
        {
            return redirect('client/checkout');    
        }
        else
        {
            return redirect('client/login')->with('message', 'Anda harus masuk untuk melanjutkan!');
        }
    }

    public function contact()
    {
        return view('contact');
    }

    public function about()
    {
        return view('about');
    }

    public function subscribe(Request $request)
    {
        $data = $this->validate($request, ['email' => 'required|email']);
        return redirect()->back()->with('message', 'Terim kasih');
    }

    public function sendMessage(Request $req)
    {
        $data = $this->validate($req, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);
        Mail::to(config('mail.username'))->send(new SendMessage($req));
        return redirect()->back()->with('message', 'Terima kasih atas partisipasi Anda');
    }
}
