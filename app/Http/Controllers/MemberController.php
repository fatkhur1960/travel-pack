<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use App\Models\Packages;
use App\Models\Confirm;
use App\Models\Invoices;
use App\Mail\VerifyEmail;
use Session;
use Auth;
use Hash;
use Mail;
use Crypt;
use Cart;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no = 1;
        $members = Member::orderBy('created_at','desc')->paginate('20');
        return view('dashboard.member', compact('members','no'));
    }

    public function client()
    {
        $no = 1;
        $id = Auth::guard('member')->user()->member_id;
        $inv = Invoices::getData($id);
        return view('client.client', compact('inv','no'));
    }

    public function checkout()
    {
        foreach(Cart::content() as $item)
        {
            $data = [
                'member_id' => Auth::guard('member')->user()->member_id,
                'package_id' => $item->id,
                'date'  => $item->options->date,
                'volume' => $item->qty,
                'total_price' => $item->subtotal
            ];
        }
        Invoices::create($data);
        Cart::destroy();
        return redirect('client');
    }

    public function upload(Request $request)
    {
        $path = 'images/confirm_files/';
        
        $file = $request->file('confirm_file');
        if($file) 
        {
            $data = [
                'invoice_id' => $request->get('invoice_id'),
                'file_location' => $path . $file->getClientOriginalName()
            ];
            $file->move($path, $file->getClientOriginalName());
            Confirm::create($data);

            return redirect('client')->with('success','Terima kasih. Permintaan Anda sedang kami proses.');
        }
        else
        {
            return redirect('client')->with('warning','Tidak ada file yang dipilih!');
        }
    }

    public function setting()
    {
        if (Auth::guard('member')->check())
        {
            return view('client.setting');
        }
        else
        {
            return redirect('client/login')
                ->with('denied', 'Anda harus masuk terlebih dahulu!');
        }
    }

    public function updatemember(Request $request)
    {
        $user = Member::find($request->get('member_id'));
        $validator = $this->validate(request(), [
            'username' => 'required|string|max:60',
            'email' => 'required|string|email|max:60',
            'firstname' => 'required|string|max:60',
            'lastname' => 'required|string|max:60'
        ]);
        
        $user->username = $request->get('username');
        $user->email = $request->get('email');
        $user->firstname = $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->password = ($request->get('password')) ? Hash::make($request->get('password')) : $user->password;
        $user->save();

        return redirect('client/setting')->with('success','Akun berhasil diupdate');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
