<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Packages;
use App\Models\Users;
use Session;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no = 1;
        $packages = Packages::orderBy('created_at','desc')->paginate('20');
        return view('dashboard.pkg', compact('packages','no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.createpkg');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validate(request(), [
            'package_name' => 'required',
            'package_price' => 'required|numeric'
        ]);

        Packages::create($data);

        return redirect('packages')->with('success','Paket baru telah disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pkg = Packages::find($id);
        return view('dashboard.editpkg', compact('pkg','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pkg = Packages::find($id);
        $this->validate(request(), [
            'package_name' => 'required',
            'package_price' => 'required|numeric'
        ]);

        $pkg->package_name = $request->get('package_name');
        $pkg->package_price = $request->get('package_price');
        $pkg->save();

        return redirect('packages')->with('success','Paket berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pkg = Packages::find($id);
        $pkg->delete();
        return redirect('packages')->with('success','Paket berhasil dihapus');
    }
}
