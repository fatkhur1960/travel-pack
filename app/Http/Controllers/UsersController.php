<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use Session;
use Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no = 1;
        $users = Users::orderBy('created_at','desc')->paginate('20');
        return view('dashboard.users', compact('users','no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.createuser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validate(request(), [
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'firstname' => 'required',
            'lastname' => 'required'
        ]);
        $data['password'] = Hash::make($request->get('password'));

        Users::create($data);

        return redirect('users')->with('success','User baru telah disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Users::find($id);
        return view('dashboard.setting', compact('id','edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Users::find($id);
        $this->validate(request(), [
            'username' => 'required',
            'email' => 'required|email',
            'firstname' => 'required',
            'lastname' => 'required'
        ]);

        $user->username = $request->get('username');
        $user->email = $request->get('email');
        $user->firstname = $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->password = ($request->get('password')) ? Hash::make($request->get('password')) : $user->password;
        $user->save();

        return redirect('dashboard/setting')->with('success','User berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Users::find($id);
        $user->delete();
        return redirect('users')->with('success','User berhasil dihapus');
    }
}
