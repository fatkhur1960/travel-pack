<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Invoices;
use Illuminate\Support\Facades\Input;
use Session;
use Hash;
use Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $no = 1;
        $data = Invoices::getAll();
        return view('dashboard.main', compact('data','no'));
    }

    public function form()
	{
		return view('signin.form');
    }

    public function confirm(Request $request)
    {
        $in = Invoices::find($request->invoice_id);
        $in->status = 'confirmed';
        $in->save();

        return redirect('dashboard')->with('success','Pembayaran berhasil dikonfirmasi');
    }

    public function attempt(Request $request) 
    {
		$this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);
        
        $attempt = [
            'username' => $request->username,
            'password' => $request->password
        ];

        if(Auth::guard('admin')->attempt($attempt, (bool) $request->remember))
        {
            return redirect('dashboard');
        }
        else
        {
            return redirect()
                ->back()
                ->withInput(Input::except('password'))
                ->withErrors([
                    'username' => 'Login gagal. Tidak ada username yang cocok!',
                    'password' => 'Login gagal. Password yang Anda masukkan salah!',
                    'status' => 'Akun Anda belum diverifikasi. Mohon periksa email Anda!'
                ]);
        }
    }
    
    public function logout() 
    {
        Auth::guard('admin')->logout();
        return redirect('/');
    }

    public function setting()
    {
        return view('dashboard.setting');
    }

}
