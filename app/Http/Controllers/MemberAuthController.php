<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Member;
use App\Mail\VerifyEmail;
use App\Mail\ResetPassword;
use Session;
use Auth;
use Hash;
use Mail;
use Validator;
use Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class MemberAuthController extends Controller
{
    public function login()
    {
        Auth::guard('member')->logout();
        return view('auth.login');
    }

    public function attempt(Request $request)
    {
        $request->merge(['status' => 'activated']);

        $this->validate($request, [
            'username' => 'required|string|max:255|unique:users',
            'password' => 'required'
        ]);

        $login = Member::whereRaw("username='{$request->username}'")->first();
        if($login)
        {
            if(Hash::check($request->password, $login->password))
            {
                $attempt = [
                    'username' => $login->username,
                    'password' => $login->password,
                    'status'   => 'activated'
                ];
                if(Auth::guard('member')->attempt($this->credentials($request), $request->filled('remember')))
                {
                    if(Session::has('redirect'))
                    {
                        return redirect(Session::get('redirect'));
                    }
                    else
                    {
                        return redirect('/client');
                    }
                }
                else 
                {
                    return redirect()
                        ->back()
                        ->withInput(Input::except('password'))
                        ->withErrors([
                            'status' => 'Login gagal. Akun Anda belum diverifikasi, mohon periksa email Anda.!'
                        ]);
                }
            }
            else
            {
                return redirect()
                ->back()
                ->withInput(Input::except('password'))
                ->withErrors([
                    'password' => 'Login gagal. Password yang Anda masukkan salah!'
                ]);
            }
        }
        else
        {
            return redirect()
                ->back()
                ->withInput(Input::except('password'))
                ->withErrors([
                    'username' => 'Login gagal. Tidak ada username yang cocok!'
                ]);
        }
    }

    public function register()
    {
        return view('auth.register');
    }

    public function send_register(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string|max:60|unique:member,username',
            'email' => 'required|string|email|max:60|unique:member,email',
            'password' => 'required|string|min:6|confirmed',
            'firstname' => 'required|string|max:60',
            'lastname' => 'required|string|max:60'
        ]);

        DB::transaction(function() use ($request) {
            $member = Member::create([
                'username' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'firstname' => $request->firstname,
                'lastname' => $request->lastname
            ]);

            Mail::to($member->email)->send(new VerifyEmail($member));
        });

        return redirect()->back()->with('message', 'Terima kasih. Silahkan cek email Anda untuk verifikasi akun.');
    }

    public function verify()
    {
        if (empty(request('token'))) {
            return redirect()->route('client.registerform');
        }

        $decryptedEmail = Crypt::decrypt(request('token'));
        $user = Member::whereRaw("email = '$decryptedEmail'")->first();

        if ($user->status == 'activated') 
        {
            // user is already active, do something
        }
        else
        {
            $user->status = 'activated';
            $user->save();
        }

        Auth::guard('member')->login($user);

        return redirect('/client');
    }

    public function logout()
    {
        Session::flush();
        Auth::guard('member')->logout();
        return redirect('/');
    }

    public function forgot()
    {
        return view('auth.passwords.email');
    }

    public function forgotAction(Request $request) 
    {
        $data = $this->validate($request, ['email' => 'required|email']);
        Mail::to($data['email'])->send(new ResetPassword($data['email']));
        return redirect()->back()->with('message', 'Silahkan periksa email Anda');
    }

    public function reset(Request $req)
    {
        $token = $req->get('token');
        if($req->get('identifier')) 
        {
            $decryptedEmail = Crypt::decrypt($req->get('identifier'));
        } 
        else 
        {
            $decryptedEmail = "";
        }
        $data = Member::whereRaw("email='$decryptedEmail' and reset_token='$token'")->first();
        if($data) 
        {
            $token = $data->reset_token;
        }
        else 
        {
            $token = "";
        }
        return view('auth.passwords.reset')->with('token', $token);
    }

    public function resetAction(Request $request)
    {
        $data = $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|confirmed'
        ]);

        $member = Member::whereRaw("email='{$data['email']}'")->first();
        $member->password = Hash::make($data['password']);
        $member->save();
        
        Auth::guard('member')->login($member);
        return redirect('/client');
    }

    protected function credentials(Request $request)
    {
        return $request->only('username', 'password', 'status');
    }
}
