<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Invoices extends Model
{
    protected $primaryKey = 'invoice_id';
    protected $fillable = [
        'member_id', 'package_id', 'date', 'volume', 'total_price', 'status'
    ];

    public static function getData($id)
    {
        return DB::table('invoices')
            ->select(DB::raw('invoices.*, packages.package_name, packages.package_price'))
            ->leftJoin('packages', 'invoices.package_id', '=', 'packages.package_id')
            ->where('member_id','=',$id)
            ->get();
    }

    public static function getAll()
    {
        return DB::table('invoices')
            ->select(DB::raw('invoices.invoice_id, invoices.date, invoices.volume, invoices.status, packages.package_name, member.email, count(confirm_id) as total, conf.file_location'))
            ->leftJoin('packages', 'invoices.package_id', '=', 'packages.package_id')
            ->leftJoin('member', 'invoices.member_id', '=', 'member.member_id')
            ->leftJoin(DB::raw('(select confirm_id, confirm.invoice_id, confirm.file_location from confirm left join invoices on confirm.invoice_id=invoices.invoice_id) as conf'), 'invoices.invoice_id', '=', 'conf.invoice_id')
            ->groupBy(DB::raw('invoices.invoice_id, invoices.date, invoices.volume, invoices.status, packages.package_name, member.email, conf.file_location'))
            ->get();
    }
}
