<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{
    protected $table = 'member';
    protected $primaryKey = 'member_id';
    protected $fillable = ['username','password','email','firstname','lastname','status'];
    protected $hidden = ['password','remember_token'];

    public function invoices()
    {
        return $this->hasOne('App\Models\Invoices');
    }
}
