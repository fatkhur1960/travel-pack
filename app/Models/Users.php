<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    protected $primaryKey = 'user_id';
    protected $fillable = ['username','password','email','firstname','lastname','remember_token'];
}
