<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $primaryKey = 'post_id';
    protected $fillable = ['post_title','post_slug','post_body','post_thumbnail'];
}
