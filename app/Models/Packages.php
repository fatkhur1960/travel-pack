<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model
{
    protected $primaryKey = 'package_id';
    protected $fillable = ['package_name','package_price'];

    public function invoices()
    {
        return $this->belongsTo('App\Models\Invoices');
    }
}
