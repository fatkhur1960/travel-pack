<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Confirm extends Model
{
    protected $table = 'confirm';
    protected $primaryKey = 'confirm_id';
    protected $fillable = ['invoice_id','file_location'];
}
