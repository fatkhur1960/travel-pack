<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@index');
Route::get('/contact-us', 'SiteController@contact');
Route::get('/about-us', 'SiteController@about');
Route::post('/send', 'SiteController@send_request')->name('send.request');
Route::post('/subscribe', 'SiteController@subscribe')->name('subscribe');
Route::post('/send-message', 'SiteController@sendMessage')->name('send.message');
Route::get('/destinasi-wisata', 'SiteController@posts')->name('view.more');
Route::get('/destinasi-wisata/{slug}', 'SiteController@viewPost');

Route::get('dashboard', 'DashboardController@index')->middleware('admin');
Route::get('dashboard/setting', 'DashboardController@setting')->middleware('admin');
Route::get('dashboard/login', 'DashboardController@form')->name('signin.form');
Route::get('dashboard/logout', 'DashboardController@logout')->middleware('admin');
Route::post('attempt', 'DashboardController@attempt')->name('signin.attempt');

Route::resource('packages', 'PackagesController')->middleware('admin');
Route::resource('users', 'UsersController')->middleware('admin');
Route::resource('posts', 'PostsController')->middleware('admin');
Route::resource('member', 'MemberController')->middleware('admin');
Route::resource('settings', 'OptionsController')->middleware('admin');
Route::post('dashboard/confirm', 'DashboardController@confirm')->name('confirm.update')->middleware('admin');

Route::get('client/login', 'MemberAuthController@login');
Route::post('login', 'MemberAuthController@attempt')->name('clientlogin.attempt');
Route::get('client/register', 'MemberAuthController@register')->name('client.registerform');
Route::post('register', 'MemberAuthController@send_register')->name('client.register');
Route::get('verify', 'MemberAuthController@verify')->name('clientregister.verify');
Route::get('client/logout', 'MemberAuthController@logout');
Route::get('client/forgot-password', 'MemberAuthController@forgot');
Route::post('forgot', 'MemberAuthController@forgotAction')->name('password.email');
Route::get('client/reset-password', 'MemberAuthController@reset')->name('client.reset');
Route::post('reset', 'MemberAuthController@resetAction')->name('password.request');

Route::get('client', 'MemberController@client')->middleware('member');
Route::get('client/checkout', 'MemberController@checkout')->middleware('member');
Route::post('client/upload', 'MemberController@upload')->name('confirm.order')->middleware('member');
Route::get('client/setting', 'MemberController@setting')->middleware('member');
Route::match(['patch', 'post'], 'update', 'MemberController@updatemember')->name('client.update');